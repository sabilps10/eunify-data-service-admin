import { object, string } from "zod";

export const loginSchema = object({
  username: string().min(1, { message: "Username is required" }),
  password: string().min(1, { message: "Password is required" })
});
