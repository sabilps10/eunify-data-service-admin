export const DRAWER_WIDTH = 320;
export const TITLE_MENU = [
  { name: "report-management", value: "report" },
  { name: "deposit-channel", value: "deposit.channel" },
  { name: "deposit-management", value: "deposit.management" },
  { name: "deposit-withdraw", value: "deposit.withdraw" },
  { name: "transaction-records", value: "transaction.records" },
  { name: "withdrawal-management", value: "withdrawal.management" },
  { name: "withdrawal-channel", value: "withdrawal.channel" },
  { name: "blacklist", value: "blacklist.channel" },
  { name: "exchange-rate", value: "exchange_rates.channel" },
  { name: "bank-&-credit-card-record", value: "bank_management" },
  { name: "user-management", value: "user_management" },
  { name: "bank-&-cc-registration-limit", value: "bank_account_limit" },
  { name: "role-settings", value: "role_setting" },
  { name: "dashboard", value: "dashboard" },
  { name: "beneficiary-accounts", value: "beneficiary" },
  { name: "list-of-key-term-naming", value: "list_keys" },
  { name: "prc-province", value: "naming_province" },
  { name: "prc-region-district", value: "naming_district" },
  { name: "prc-city", value: "naming_city" },
  { name: "audit-log", value: "audit_log" },
  { name: "reports", value: "reconciliation_report" },
  { name: "edda-record", value: "edda" },
  { name: "payment-management", value: "payment" },
  { name: "dummy-account-list", value: "dummy_account" },
  { name: "ccba-bank-records", value: "ccba_records" },
  { name: "manual-adjustments-types", value: "manual_adjustment_type" },
  { name: "local-bank-list-edda-mapping", value: "local_bank_edda" },
  { name: "exceptions-management", value: "exceptions" },
  { name: "notification", value: "notification" },
  { name: "trade-date", value: "trade_date" },
  { name: "operator-notifications", value: "operator_notifications" }
];
export const AVAILABLE_OPERATION_TIME_DAY = [
  { name: "sun", value: 7 },
  { name: "mon", value: 1 },
  { name: "tue", value: 2 },
  { name: "wed", value: 3 },
  { name: "thu", value: 4 },
  { name: "fri", value: 5 },
  { name: "sat", value: 6 }
];

export const LOCALSTORAGE_KEY = {
  TOKEN: "access-token",
  REFRESH_TOKEN: "refresh-token",
  ACCOUNT: "account",
  EXPIRED_TOKEN: "expired_token",
  EXPIRED_TIME: "expired_time"
};

export const AML_CODE: any = {
  M001: "Pass",
  M002: "Fail",
  M003: "Not Found"
};
