export const REPORT_HEADER = [
    {
      id: "id",
      numeric: false,
      disablePadding: false,
      label: "ID",
      isSort: true
    },
    {
      id: "type",
      numeric: false,
      disablePadding: false,
      label: "Type",
      isSort: true
    },
    {
      id: "applicationCode",
      numeric: true,
      disablePadding: false,
      label: "App Code",
      isSort: true
    },
    {
      id: "createdDate",
      numeric: false,
      disablePadding: false,
      label: "Date",
      isSort: true
    },
    {
      id: "toRecipients",
      numeric: false,
      disablePadding: false,
      label: "Recipients",
      isSort: false
    },
    {
      id: "scheduleType",
      numeric: false,
      disablePadding: false,
      label: "Schedule Type",
      isSort: false
    },
    {
      id: "scheduledDateTime",
      numeric: false,
      disablePadding: false,
      label: "Schedule Date Time",
      isSort: false
    },
    {
      id: "status",
      numeric: true,
      disablePadding: false,
      label: "Status",
      isSort: true
    },
    {
      id: "retryCount",
      numeric: false,
      disablePadding: false,
      label: "Retry Count",
      isSort: false
    },
    {
      id: "action",
      numeric: false,
      disablePadding: false,
      label: "Action",
      key_value: "id",
      isSort: true,
      idValue: "id"
    }
  ];
  
  export const REPORT_ACTION = [{name: "retry-selected-notification", action: "retry-notification"}];
  
  export const REPORT_EXPORT_HEADING = [
    { name: "id", label: "ID", width: 30, position: 1 },
    { name: "type", label: "Type", width: 20, position: 2 },
    { name: "applicationCode", label: "App Code", width: 20, position: 4 },
    { name: "displayCreatedDate", label: "Date", width: 20, position: 5 },
    { name: "status", label: "Status", width: 30, position: 6 },
  ];
  