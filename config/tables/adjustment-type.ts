export const ADJUSTMENT_TYPE_HEADER = [
  {
    id: "codeCode",
    numeric: false,
    row_condition: [
      {
        type: "contain-string",
        key: "MI",
        value: "Deposit"
      },
      {
        type: "contain-string",
        key: "MO",
        value: "Withdrawal"
      }
    ],
    disablePadding: false,
    label: "Type",
    isSort: true
  },
  {
    id: "nameEn",
    numeric: false,
    disablePadding: false,
    label: "Adjustment Type",
    isSort: true
  },
  {
    id: "active",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "nameEn",
    idValue: "codeCode"
  }
];

export const ADJUSTMENT_TYPE_ACTION = [{ name: "add", action: { type: "link", value: "/add" } }];
