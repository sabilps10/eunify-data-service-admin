export const PAYMENT_REJECT_DEPOSIT_ACTION = [
  { name: "export", action: { type: "export_csv", value: "" } },
  { name: "export_fps", action: { type: "callback_action", value: "export-transaction" } },
  { name: "payment_completed", action: { type: "callback_action", value: "payment-confirm" } },
  { name: "payment_failed", action: { type: "callback_action", value: "payment-failed" } }
];

export const PAYMENT_MANAGEMENT_HEADER = [
  {
    id: "checkbox",
    numeric: true,
    disablePadding: false,
    isSort: false
  },
  {
    id: "orderNo",
    numeric: true,
    disablePadding: false,
    label: "Reference ID",
    isSort: true
  },
  {
    id: "entityId",
    numeric: false,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "paymentChannelName",
    numeric: true,
    disablePadding: false,
    label: "Deposit Channel",
    isSort: false
  },
  {
    id: "custom_value",
    name: "settle_currency",
    numeric: false,
    disablePadding: false,
    label: "Settlement Currency",
    isSort: false
  },
  {
    id: "custom_value",
    name: "settlement_amount",
    numeric: true,
    disablePadding: false,
    label: "Settlement Amount",
    isSort: false
  },
  {
    id: "createdDate",
    numeric: false,
    disablePadding: false,
    label: "Application Time",
    isSort: true
  },
  {
    id: "tradingAccountNo",
    numeric: true,
    disablePadding: false,
    label: "Trading Account Number",
    isSort: true
  },
  {
    id: "paymentStatusName",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true,
    sortId: "paymentStatus"
  },

  {
    id: "exported",
    numeric: false,
    disablePadding: false,
    label: "Export for FPS?",
    row_condition: [
      {
        type: "contain-string",
        key: "true",
        value: "Y"
      },
      {
        type: "contain-string",
        key: "false",
        value: "N"
      }
    ],
    isSort: false
  },
  {
    id: "exportedDate",
    numeric: false,
    disablePadding: false,
    label: "FPS Export Date",
    isSort: false
  },
  {
    id: "comment",
    numeric: false,
    disablePadding: false,
    label: "Remarks",
    isSort: false
  },
  {
    id: "action",
    numeric: true,
    disablePadding: false,
    label: "Action",
    key_value: "requestId",
    isSort: false,
    idValue: "id"
  }
];
