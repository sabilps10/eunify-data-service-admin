export const CCBA_MANAGEMENT_ROWS = [
  "entity",
  "efsg_bank",
  "efsg_bank_account_num",
  "bank_transaction_ref_num",
  "transaction_date",
  "client_bank_acc_num",
  "client_bank_acc_name",
  "actual_currency",
  "actual_amount",
  "platform_currency",
  "platform_amount",
  "trading_acc_num",
  "trading_acc_name",
  "reference_number",
  "status"
];
export const CCBA_MANAGEMENT_HEADER = [
  {
    id: "entityId",
    numeric: true,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "efsgBankName",
    numeric: true,
    disablePadding: false,
    label: "EFSG Bank",
    isSort: false
  },
  {
    id: "efsgBankAccNo",
    numeric: true,
    disablePadding: false,
    label: "EFSG Bank Account Number",
    isSort: false
  },
  {
    id: "refId",
    numeric: false,
    disablePadding: false,
    label: "Bank Transaction Reference Number",
    isSort: false
  },
  {
    id: "txnDate",
    numeric: false,
    disablePadding: false,
    label: "Transaction Date & Time",
    isSort: true
  },
  {
    id: "bankAccountNo",
    numeric: false,
    disablePadding: false,
    label: "Client Bank Account Number",
    isSort: false
  },
  {
    id: "accHolderName",
    numeric: true,
    disablePadding: false,
    label: "Client Bank Account Name",
    isSort: false
  },
  {
    id: "custom_value",
    name: "actual_currency",
    numeric: false,
    disablePadding: false,
    label: "Actual Currency",
    isSort: true,
    sortId: "currency"
  },
  {
    id: "custom_value",
    name: "actual_amount",
    numeric: false,
    disablePadding: false,
    label: "Actual Amount",
    isSort: false
  },
  {
    id: "custom_value",
    name: "platform_amount",
    numeric: false,
    disablePadding: false,
    label: "Platform Amount",
    isSort: false
  },
  {
    id: "tradingAccountNo",
    numeric: false,
    disablePadding: false,
    label: "Trading Account Number",
    isSort: true
  },
  {
    id: "clientName",
    numeric: false,
    disablePadding: false,
    label: "Trading Account Name",
    isSort: true
  },
  {
    id: "orderNo",
    numeric: false,
    disablePadding: false,
    label: "Reference Number",
    isSort: true
  },
  {
    id: "paymentStatusName",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true,
    sortId: "paymentStatus"
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "orderNo",
    idValue: "id"
  }
];

export const CCBA_MANAGEMENT_ACTION = [{ name: "export", action: { type: "export_csv", value: "" } }];

export const CCBA_EXPORT_HEADING = [
  { name: "entityId", label: "Entity (Bank)", width: 20, position: 1 },
  { name: "efsgBankName", label: "EFSG Bank", width: 20, position: 2 },
  { name: "efsgBankAccNo", label: "EFSG Bank Acc No", width: 20, position: 3 },
  { name: "refId", label: "Bank TxN Ref No", width: 20, position: 4 },
  { name: "txnDate", label: "TxN Date Time", width: 20, position: 5 },
  { name: "bankAccountNo", label: "Client Bank Acc No", width: 20, position: 6 },
  { name: "accHolderName", label: "Client Bank Acc Holder Name", width: 20, position: 7 },
  { type: "custom-data", key: "actual-currency", label: "Actual Currency", width: 20, position: 8 },
  { type: "custom-data", key: "actual-amount", label: "Actual Amount", width: 20, position: 9 },
  { type: "custom-data", key: "platform-currency", label: "Platform Currency", width: 20, position: 10 },
  { type: "custom-data", key: "platform-amount", label: "Platform Amount", width: 20, position: 11 },
  { name: "tradingAccountNo", label: "Trading Acc No", width: 20, position: 12 },
  { name: "clientName", label: "Trading Acc Name", width: 20, position: 13 },
  { name: "orderNo", label: "Reference No", width: 30, position: 14 },
  { name: "paymentStatusName", label: "Status", width: 20, position: 15 }
];
