export const KEY_CRYPTO_HEADER = [
  {
    id: "codeCode",
    numeric: false,
    disablePadding: false,
    label: "System Name",
    isSort: true
  },
  {
    id: "nameEn",
    numeric: false,
    disablePadding: false,
    label: "Eng Display Name",
    isSort: true
  },
  {
    id: "nameTc",
    numeric: false,
    disablePadding: false,
    label: "Trad Chi Display Name",
    isSort: true
  },
  {
    id: "nameSc",
    numeric: false,
    disablePadding: false,
    label: "Simpli Chi Display Name",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "codeCode",
    idValue: "codeCode"
  }
];
