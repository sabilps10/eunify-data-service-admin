export const WITHDRAWAL_RECORD_FILTER = [
  {
    name: "type",
    id: "type",
    type: "dropdown",
    options: []
  },
  {
    name: "adjustment_type",
    id: "adjustment_type",
    type: "dropdown",
    options: []
  },
  {
    name: "entity",
    id: "entity",
    type: "dropdown",
    options: []
  },
  {
    name: "order_no",
    id: "order_no",
    type: "text"
  },
  {
    name: "aml_status",
    id: "aml_status",
    type: "dropdown",
    options: []
  },
  {
    name: "settlement_currency",
    id: "settlement_currency",
    type: "dropdown",
    options: []
  },
  {
    name: "trading_acc",
    id: "trading_acc",
    type: "text"
  },
  {
    name: "acc_holder",
    id: "acc_holder",
    type: "text"
  },
  {
    name: "withdrawal_channel",
    id: "withdrawal_channel",
    type: "dropdown",
    options: []
  },
  {
    name: "application_time",
    id: "application_time",
    type: "time-range"
  },
  {
    name: "status",
    id: "status",
    type: "dropdown",
    options: []
  }
];

export const WITHDRAWAL_ROWS = [
  "type",
  "adjustment_type",
  "ref_no",
  "entity",
  "trading_acc",
  "acc_holder",
  "aml_status",
  "withdrawable_balance",
  "latest_info",
  "platform_amount",
  "actual_currency",
  "ref_settlement_amount",
  "withdrawal_channel",
  "application_time",
  "approval_time",
  "request_by",
  "remarks",
  "status"
];

export const WITHDRAWAL_RECORD_HEADER = [
  {
    id: "tradeTypeName",
    numeric: false,
    disablePadding: false,
    label: "Type",
    isSort: true,
    sortId: "tradeType"
  },
  {
    id: "adjustmentTypeName",
    numeric: false,
    disablePadding: false,
    label: "Adjustment Type",
    isSort: true,
    sortId: "adjustmentType"
  },
  {
    id: "orderNo",
    numeric: true,
    disablePadding: false,
    label: "Reference Number",
    isSort: true
  },
  {
    id: "entityId",
    numeric: false,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "tradingAccountNo",
    numeric: true,
    disablePadding: false,
    label: "Trading Account",
    isSort: true
  },
  {
    id: "clientName",
    numeric: false,
    disablePadding: false,
    label: "Trading Account Holder",
    isSort: false
  },
  {
    id: "isValidAml",
    numeric: false,
    disablePadding: false,
    label: "AML Status",
    isSort: true,
    sortId: "validAml"
  },
  {
    id: "custom_value",
    name: "balance",
    numeric: true,
    disablePadding: false,
    label: "Withdrawable Balance",
    isSort: true,
    sortId: "balance"
  },
  {
    id: "latest_info",
    numeric: false,
    disablePadding: false,
    label: "Latest Info",
    isSort: false
  },
  {
    id: "custom_value",
    name: "platform_amount",
    numeric: false,
    disablePadding: false,
    label: "Platform Amount",
    isSort: true,
    sortId: "amount"
  },
  {
    id: "settlementCurrency",
    numeric: false,
    disablePadding: false,
    label: "Actual Currency",
    isSort: true
  },
  {
    id: "custom_value",
    name: "actual_amount",
    numeric: false,
    disablePadding: false,
    label: "Actual Amount",
    isSort: true,
    sortId: "depositAmount"
  },
  {
    id: "paymentChannelName",
    numeric: false,
    disablePadding: false,
    label: "Withdrawal Channel",
    isSort: true
  },
  {
    id: "createDate",
    numeric: false,
    disablePadding: false,
    label: "Application Time",
    isSort: true
  },
  {
    id: "approvedDate",
    numeric: false,
    disablePadding: false,
    label: "Approval Time",
    isSort: true
  },
  {
    id: "requestBy",
    numeric: false,
    disablePadding: false,
    label: "Request By",
    isSort: false
  },
  {
    id: "comment",
    numeric: false,
    disablePadding: false,
    label: "Remarks",
    isSort: false
  },
  {
    id: "statusName",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true,
    sortId: "status"
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "orderNo",
    sticky: true,
    isSort: false,
    idValue: "id"
  }
];

export const WITHDRAWAL_RECORD_ACTION = [
  { name: "add", action: { type: "link", value: "/add" } },
  { name: "export", action: { type: "export_csv", value: "" } }
];

export const WITHDRAWAL_EXPORT_HEADING = [
  { name: "tradeTypeName", label: "Type", width: 20, position: 1 },
  { name: "adjustmentTypeName", label: "Adjustment Type", width: 20, position: 2 },
  { name: "orderNo", label: "Reference No", width: 20, position: 3 },
  { name: "entityId", label: "Entity", width: 20, position: 4 },
  { name: "tradingAccountNo", label: "Trading Acc No", width: 20, position: 5 },
  { name: "clientName", label: "Acc Holder Name", width: 20, position: 6 },
  {
    name: "isValidAml",
    label: "AML Status",
    type: "custom-value",
    custom_values: [
      { value: true, label: "Pass" },
      { value: false, label: "Fail" }
    ],
    width: 20,
    position: 7
  },
  { name: "balance", label: "Withdrawal Balance", width: 20, position: 8 },
  { type: "custom-data", key: "platform-amount", label: "Platform Amount", width: 20, position: 9 },
  { type: "custom-data", key: "exchange-rate", label: "Ref Exchange rate", width: 20, position: 10 },
  { name: "settlementCurrency", label: "Actual Currency", width: 20, position: 11 },
  { type: "custom-data", key: "actual-amount", label: "Actual Amount", width: 20, position: 12 },
  { type: "custom-data", key: "service-charge", label: "Ref Service Charge", width: 20, position: 13 },
  // {
  //   type: "custom-data",
  //   key: "calculated-actual-deposit",
  //   label: "Calculated Actual Deposit Amount",
  //   width: 20,
  //   position: 14
  // },
  { name: "paymentChannelName", label: "Withdrawal Channel", width: 20, position: 14 },
  { name: "bankName", label: "Bank Name", width: 20, position: 15 },
  { name: "bankAccountNo", label: "Bank Account No", width: 20, position: 16 },
  { name: "accHolderName", label: "Bank Account Holder Name", width: 20, position: 17 },
  {
    type: "custom-data",
    key: "bank-location",
    name: "bankLocation",
    label: "Bank Location",
    width: 20,
    position: 18
  },
  {
    type: "custom-data",
    key: "bank-province",
    name: "bankProvince",
    label: "Bank Province",
    width: 20,
    position: 19
  },
  {
    type: "custom-data",
    key: "bank-region",
    name: "bankRegion",
    label: "Bank Region",
    width: 20,
    position: 20
  },
  { type: "custom-data", key: "bank-city", name: "bankCity", label: "Bank City", width: 20, position: 21 },
  { name: "bankAddress", label: "Bank Address", width: 20, position: 22 },
  { name: "IBAccountNo", label: "IB Code", width: 20, position: 23 },
  { name: "cryptoNetwork", label: "Crypto Network", width: 20, position: 24 },
  { name: "cryptoAddress", label: "Crypto Address", width: 20, position: 25 },
  // { name: "tradeDate", label: "Trade Date", width: 20, position: 27 },
  { name: "createDate", label: "Application time", width: 20, position: 26 },
  { name: "approvedDate", label: "Approval Time", width: 20, position: 27 },
  { name: "requestBy", label: "Request by", width: 20, position: 28 },
  { name: "approvedBy", label: "Approved by", width: 20, position: 29 },
  { name: "comment", label: "Remarks", width: 20, position: 30 },
  { name: "statusName", label: "Status", width: 20, position: 31 }
];
