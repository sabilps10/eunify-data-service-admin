export const PAYMENT_APPROVED_WITHDRAWAL_ACTION = [
  { name: "export", action: { type: "export_csv", value: "" } },
  { name: "export_fps", action: { type: "callback_action", value: "export-transaction" } },
  { name: "payment_completed", action: { type: "callback_action", value: "payment-confirm" } },
  { name: "payment_failed", action: { type: "callback_action", value: "payment-failed" } },
  { name: "payment_cancelled", action: { type: "callback_action", value: "payment-cancel" } }
];

export const PAYMENT_APPROVED_WITHDRAWAL_HEADER = [
  {
    id: "no",
    numeric: true,
    disablePadding: false,
    label: "No.",
    isSort: true
  },
  {
    id: "reference_id",
    numeric: true,
    disablePadding: false,
    label: "Reference No.",
    isSort: true
  },
  {
    id: "deposit_channel",
    numeric: true,
    disablePadding: false,
    label: "Deposit Channel",
    isSort: true
  },
  {
    id: "custom_value",
    name: "settlement_currency",
    numeric: false,
    disablePadding: false,
    label: "Settlement Currency",
    isSort: true
  },
  {
    id: "custom_value",
    name: "actual_currency",
    numeric: true,
    disablePadding: false,
    label: "Actual Amount",
    isSort: true
  },
  {
    id: "application_time",
    numeric: false,
    disablePadding: false,
    label: "Application Time",
    isSort: true
  },
  {
    id: "trading_acc_no",
    numeric: true,
    disablePadding: false,
    label: "Trading Account Number",
    isSort: true
  },
  {
    id: "status",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true
  },
  {
    id: "export_fps",
    numeric: false,
    disablePadding: false,
    label: "Export for FPS?",
    isSort: true
  },
  {
    id: "fpx_export_date",
    numeric: false,
    disablePadding: false,
    label: "FPS Export Date",
    isSort: true
  },
  {
    id: "remark",
    numeric: false,
    disablePadding: false,
    label: "Remarks",
    isSort: true
  },
  {
    id: "action",
    numeric: true,
    disablePadding: false,
    label: "Action",
    key_value: "reference_id",
    isSort: true,
    idValue: "id"
  }
];

export const PAYMENT_APPROVED_WITHDRAWAL_ROWS = [
  "reference_id",
  "deposit_channel",
  "settlement_currency",
  "settlement_amount",
  "application_time",
  "trading_acc_no",
  "status",
  "export_fps",
  "fps_export_date",
  "remark"
];
