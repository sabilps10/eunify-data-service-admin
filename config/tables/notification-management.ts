export const NOTIFICATION_MANAGEMENT_HEADER = [
  {
    id: "no",
    numeric: true,
    disablePadding: false,
    label: "No",
    isSort: true
  },
  {
    id: "eventId",
    numeric: false,
    disablePadding: false,
    label: "Event Id",
    isSort: true
  },
  {
    id: "description",
    numeric: false,
    disablePadding: false,
    label: "Description",
    isSort: true
  },
  {
    id: "subject",
    numeric: false,
    disablePadding: false,
    label: "Subject",
    isSort: true
  },
  {
    id: "type",
    numeric: false,
    disablePadding: false,
    label: "Type",
    isSort: true
  },
  {
    id: "action",
    numeric: true,
    disablePadding: false,
    label: "Action",
    key_value: "eventId",
    idValue: "eventId"
  }
];

export const NOTIFICATION_MANAGEMENT_ACTION = [{ name: "add", action: { type: "link", value: "/add" } }];
