export const USER_MANAGEMENT_ACTION = [
  { name: "add", action: { type: "link", value: "/add" } },
  { name: "export", action: { type: "export_csv", value: "" } }
];
export const USER_MANAGEMENT_HEADER = [
  {
    id: "userName",
    numeric: false,
    disablePadding: false,
    label: "Name",
    isSort: true
  },
  {
    id: "email",
    numeric: false,
    disablePadding: false,
    label: "Email",
    isSort: true
  },
  {
    id: "isActive",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "userName",
    isSort: true,
    idValue: "userName"
  }
];

export const USER_MANAGEMENT_EXPORT_HEADING = [
  { name: "userName", label: "Name", width: 20, position: 1 },
  { name: "email", label: "Email", width: 20, position: 2 },
  { name: "isActive", label: "Status", width: 20, position: 3, type: "custom-columns-user" },
  {
    name: "roleList",
    type: "custom-columns-user",
    position: 4
  }
];
