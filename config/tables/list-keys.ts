import { KEY_CRYPTO_HEADER } from "./key-crypto";
import { KEY_CURRENCY_HEADER } from "./key-currency";
import { KEY_EDDA_NAME_HEADER } from "./key-edda-name";
import { KEY_ENTITY_HEADER } from "./key-entity";

export const MULTIPLE_LIST_KEY_TABLE = [
  {
    name: "entity",
    tables: {
      header: KEY_ENTITY_HEADER,
      tableData: "entities.data",
      isLoadingData: "entities.isLoading"
    }
  },
  {
    name: "edda_bank_display_name",
    tables: {
      header: KEY_EDDA_NAME_HEADER,
      tableData: "eddaDisplayNames.data",
      isLoadingData: "eddaDisplayNames.isLoading"
    }
  }
  // {
  //   name: "crypto_network",
  //   tables: {
  //     header: KEY_CRYPTO_HEADER,
  //     tableData: "networks.data",
  //     isLoadingData: "networks.isLoading"
  //   }
  // }
];
