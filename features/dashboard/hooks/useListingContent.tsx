import { useState } from "react";

const useListingContent = () => {
  const [tableData, setTableData] = useState<any>([]);
  const [tableIsLoading, setTableIsLoading] = useState<boolean>(false);

  const setTableValue = (name: string, value: any) => {
    if (name == "table-data") {
      setTableData(value);
    }
    if (name == "table-loading") {
      setTableIsLoading(value);
    }
  };

  return {
    tableData,
    tableIsLoading,
    setTableValue
  };
};

export default useListingContent;
