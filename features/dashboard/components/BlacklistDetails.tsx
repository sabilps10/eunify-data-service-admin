import { FC, useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";

import {
  Typography,
  Stack,
  Card,
  CardContent,
  Box,
  Divider,
  Grid,
  Button,
  FormLabel,
  FormControlLabel,
  Checkbox,
  FormControl
} from "@mui/material";
import useTranslate from "@/hooks/useTranslate";
import { FormProvider, useForm, SubmitHandler } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { TypeOf } from "zod";
import InputText from "@/components/FormFields/InputText";
import InputSelect from "@/components/FormFields/InputSelect";
import { withStyles } from "@mui/styles";
import securityStore, { SecurityStoreTypes } from "@/store/securityStore";
import shallow from "zustand/shallow";
import convertNameToUrl from "@/utils/convertToPathUrl";
import toast from "react-hot-toast";

const StyledCheckbox = withStyles({
  root: {
    "&$disabled": {
      color: "hsla(189, 100%, 41%, 1)"
    }
  },
  checked: {},
  disabled: {}
})(Checkbox);

const BlacklistDetail: FC<{
  validations: any;
  initialData?: any;
  entity: any;
  options: any[];
  onSubmit?: (value: any) => void;
  onCancel?: () => void;
  requestError?: any;
  loadingButton?: boolean;
  parentCallback: any;
}> = ({
  validations,
  initialData,
  onSubmit,
  entity,
  options = [],
  requestError = null,
  onCancel = () => null,
  loadingButton = false,
  parentCallback = () => null
}) => {
  const {
    query: { id, feature, page, entity: queryEntity },
    push
  } = useRouter();
  const { translate } = useTranslate();
  const methods = useForm<any>({
    resolver: zodResolver(validations)
  });
  const {
    handleSubmit,
    getValues,
    setValue,
    watch,
    formState: { errors }
  } = methods;

  const { client_functions } = securityStore((state) => state, shallow) as SecurityStoreTypes;

  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const [blacklist, setBlacklist] = useState<any>([]);

  const tradingAccNo = watch("trading_acc_no");

  type FormInput = TypeOf<typeof validations>;

  useEffect(() => {
    if (initialData) {
      for (let key in initialData) {
        if (initialData[key]) {
          setValue(key, initialData[key]);
        }

        if (key === "blacklist_channel") {
          setBlacklist(initialData[key]);
        }
      }
    }
  }, [initialData]);

  const onSubmitHandler: SubmitHandler<FormInput> = (data) => {
    const isHaveAccess = client_functions[data.entity].find(
      (menu: any) => convertNameToUrl(menu.funcNameEn) == feature
    ).writeAccess;

    if (onSubmit) {
      if (isHaveAccess) {
        onSubmit({ ...data, blacklist_channel: blacklist });
      } else {
        toast.error(`Dont have write access for this entity`, {
          duration: 5000,
          position: "top-right",
          style: {
            background: "#ffe3e3",
            color: "#000000"
          }
        });
      }
    }
  };

  const handleCheck = (e: any, val: any) => {
    const find = blacklist?.find((el: any) => el.value == val.value);

    const index = options.findIndex((el: any) => el.value == val.value);

    options[index] = { ...val, checked: e.target.checked };
    forceUpdate();

    if (!e.target.checked) {
      const copy = [...blacklist];
      const filtered = copy.filter((el) => el.value !== find.value);
      setValue("blacklist_channel", filtered.length > 0 ? filtered : undefined);
      setBlacklist(filtered.length > 0 ? filtered : undefined);
    } else {
      setValue(
        "blacklist_channel",
        blacklist
          ? [...blacklist, { ...val, checked: e.target.checked }]
          : [{ ...val, checked: e.target.checked }]
      );
      setBlacklist(
        blacklist
          ? [...blacklist, { ...val, checked: e.target.checked }]
          : [{ ...val, checked: e.target.checked }]
      );
    }
  };

  const callback = useCallback(() => {
    parentCallback(getValues(), methods);
  }, []);

  useEffect(() => {
    if (tradingAccNo) {
      callback();
    }
  }, [tradingAccNo]);

  return (
    <Stack>
      <Card sx={{ mb: 10 }}>
        <FormProvider {...methods}>
          <Stack sx={{ p: 2 }}>
            <Typography variant="h4">{translate(`dashboard.labels.${page}`)}</Typography>
          </Stack>
          <Divider />
          <Box component="form" noValidate autoComplete="off" onSubmit={handleSubmit(onSubmitHandler)}>
            <CardContent>
              <Grid container spacing={2}>
                <Grid sm={6} item>
                  <InputSelect
                    defaultValue={initialData ? initialData["entity"] : ""}
                    options={entity}
                    name="entity"
                    fullWidth={true}
                    label={translate(`dashboard.forms.labels.entity`)}
                    disabled={page === "add" ? false : true}
                    set={setValue}
                    addQuery={true}
                  />
                </Grid>
                <Grid sm={6} item>
                  <InputText
                    defaultValue={initialData ? initialData["trading_acc_no"] : ""}
                    type="text"
                    name="trading_acc_no"
                    fullWidth={true}
                    label={translate(`dashboard.forms.labels.trading_acc_no`)}
                    disabled={page === "add" ? (watch("entity") ? false : true) : true}
                    set={setValue}
                    get={getValues}
                    addQuery={true}
                  />
                </Grid>
                <Grid sm={6} item>
                  <InputText
                    defaultValue={initialData ? initialData["trading_acc_name"] : ""}
                    type="text"
                    name="trading_acc_name"
                    fullWidth={true}
                    label={translate(`dashboard.forms.labels.trading_acc_name`)}
                    disabled={true}
                    set={setValue}
                    get={getValues}
                  />
                </Grid>
                <Grid sm={12} item>
                  <FormControl sx={{ gap: 1 }} fullWidth error={!!errors["blacklist_channel"]}>
                    <FormLabel color="secondary">Blacklist Channel</FormLabel>
                    <Stack direction="row" alignItems="center" sx={{ flexWrap: "wrap" }} gap={2}>
                      {options
                        .filter((el) => el.entity === (queryEntity ? queryEntity : initialData?.entity))
                        .map((val: any, key: number) => {
                          return (
                            <FormControlLabel
                              label={val.label}
                              key={key}
                              control={
                                <StyledCheckbox
                                  disabled={loadingButton}
                                  onChange={(e) => handleCheck(e, val)}
                                />
                              }
                              checked={val.checked ? true : false}
                            />
                          );
                        })}
                    </Stack>
                    {errors["blacklist_channel"] && (
                      <Typography
                        variant="body"
                        color="brandRed.500"
                      >{`${errors["blacklist_channel"].message}`}</Typography>
                    )}
                  </FormControl>
                </Grid>
              </Grid>
            </CardContent>
            <Divider />
            <Stack direction={"row"} gap={2} justifyContent={"flex-end"} sx={{ pr: 2 }}>
              <Button
                onClick={function () {
                  onCancel();
                  push(`/${feature}/management`);
                }}
                sx={{ mt: requestError ? 2 : 3, mb: 2 }}
                variant={"outlined"}
              >
                <Typography variant="body" fontWeight={700}>
                  {translate("dashboard.buttons.cancel")}
                </Typography>
              </Button>
              <Button
                type="submit"
                sx={{ mt: requestError ? 2 : 3, mb: 2 }}
                disabled={loadingButton || Object.keys(errors).length > 0}
              >
                <Typography variant="body" fontWeight={700}>
                  {translate(`dashboard.buttons.submit`)}
                </Typography>
              </Button>
            </Stack>
          </Box>
        </FormProvider>
      </Card>
    </Stack>
  );
};

export default BlacklistDetail;
