import { FC } from "react";

import { Typography, Stack, Breadcrumbs, Card } from "@mui/material";
import { useRouter } from "next/router";
import useTranslate from "@/hooks/useTranslate";
import { snakeTotitleCaseText } from "../../../config/utilities";
import { TITLE_MENU } from "@/config/constants";

const DashboardTitle: FC = () => {
  const { query } = useRouter();
  const { translate } = useTranslate();

  /* note: add every feature title menu in  @/config/constants */
  const feature = TITLE_MENU.find((el: any) => el.name === query.feature);
  /* end of note*/

  const RenderPageCrumb = () => {
    if (query.page == "management") {
      return (
        <Typography variant="h4" fontWeight={700}>
          {feature && translate(`dashboard.labels.management`)}
        </Typography>
      );
    }
    if (query.page == "dashboard") {
      return (
        <Typography variant="h4" fontWeight={700}>
          {feature && translate(`dashboard.labels.dashboard`)}
        </Typography>
      );
    }
    if (query.page == "edit") {
      return (
        <Typography variant="h4" fontWeight={400}>
          {translate(`dashboard.labels.edit`)}
        </Typography>
      );
    }
    if (query.page == "notification-preview") {
      return (
        <Typography variant="h4" fontWeight={400}>
          {translate(`dashboard.labels.preview`)}
        </Typography>
      );
    }
    if (query.page == "detail") {
      return (
        <Typography variant="h4" fontWeight={400}>
          {translate(`dashboard.labels.detail`)}
        </Typography>
      );
    }
    if (query.page == "add") {
      return (
        <Typography variant="h4" fontWeight={700}>
          {translate(`dashboard.labels.add`)}
        </Typography>
      );
    }

    return <></>;
  };
  const RenderDetailCrumb = () => {
    if (query.id) {
      return (
        <Typography variant="h4" fontWeight={700}>
          {query.id}
        </Typography>
      );
    }

    return <></>;
  };

  const RenderTabCrumb = () => {
    if (query.tab) {
      return (
        <Typography variant="h4" fontWeight={700}>
          {typeof query.tab == "string" && snakeTotitleCaseText(query.tab)}
        </Typography>
      );
    }

    return <></>;
  };
  return (
    <Stack>
      <Breadcrumbs separator="›">
        <Typography variant="h4" fontWeight={400}>
          {feature && translate(`dashboard.menu.${feature?.value}.title`)}
        </Typography>
        {query.page && <RenderPageCrumb />}
        {query.id && <RenderDetailCrumb />}
        {query.tab && <RenderTabCrumb />}
      </Breadcrumbs>
    </Stack>
  );
};

export default DashboardTitle;
