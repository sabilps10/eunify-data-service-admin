import { FC } from "react";
import { FormLabel, Stack } from "@mui/material";
//@ts-ignore
import FasterPaymentSystemQRCode from "react-fps-hk-qrcode";

const FPSQrCode = ({ value }: { value?: any }) => {
  return (
    <Stack>
      <FormLabel sx={{ mb: 2 }}>Qr Code</FormLabel>
      <FasterPaymentSystemQRCode
        account="04"
        bank_code={value.bankCode}
        fps_id={value.bankAccNo}
        email=""
        mcc="0000"
        currency="344"
        amount=""
        reference=""
      />
    </Stack>
  );
};

export default FPSQrCode;
