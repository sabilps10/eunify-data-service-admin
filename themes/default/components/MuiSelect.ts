import { Components } from "@mui/material";

import { palette } from "@themes/default/colors";
import { spacing } from "@themes/default/spacing";
import { typography } from "@themes/default/typography";

const MuiSelect: Components["MuiSelect"] = {
  defaultProps: {
    MenuProps: {
      PaperProps: {
        sx: {
          marginTop: spacing(0.5),
          borderRadius: spacing(0.5)
        }
      },
      anchorOrigin: {
        horizontal: "right",
        vertical: "bottom"
      },
      transformOrigin: {
        horizontal: "right",
        vertical: "top"
      }
    }
  },
  styleOverrides: {
    filled: {
      padding:"16.5px 14px",
      background: palette.brandGrey[500],
      borderRadius: "5px",
      border: "none",
      ...typography.bigTextB
      }
  }
};

export default MuiSelect;
