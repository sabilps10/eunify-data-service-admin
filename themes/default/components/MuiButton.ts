import { Components, alpha } from "@mui/material";

import { palette } from "@themes/default/colors";
import { spacing } from "@themes/default/spacing";
import { typography } from "@themes/default/typography";

const MuiButton: Components["MuiButton"] = {
  styleOverrides: {
    root: {
      padding: spacing(1.5, 3),
      backgroundColor: palette.brandYellow[500],
      color: palette.neutrals.text,
      borderRadius: "25px",
      textTransform: "none",
      ":hover": {
        backgroundColor: alpha(palette.brandYellow[500], 0.8)
      }
    },
    textSizeLarge: {
      ...typography.h4
    },
    outlined: {
      color: palette.brandYellow[200],
      background: "none",
      borderWidth: 2,
      borderColor: palette.brandYellow[200],
      ":hover, :active": {
        borderWidth: 2,
        borderColor: palette.brandYellow[200],
        background: "none"
      }
    },
    outlinedSizeLarge: {
      ...typography.h4,
      color: palette.brandYellow[500]
    }
  }
};

export default MuiButton;
