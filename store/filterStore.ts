import createStore from "zustand";

type FilterState = {
  filter: any;
};

type FilterAction = {
  setFilter: (value: any) => void;
};

export type FilterStoreTypes = FilterState & FilterAction;

const filterStore = createStore<FilterStoreTypes>((set) => ({
  filter: null,
  setFilter: async (value: any) => {
    set(() => ({
      filter: value
    }));
  }
}));

export default filterStore;
