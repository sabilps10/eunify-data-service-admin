import { useState } from "react";
import { FilterPagination, Pagination } from "@/types";

type Paginationparams<T> = {
  filter: T;
} & FilterPagination;

function usePagination<T>(filter: T) {
  const [filterPagination, setFilterPagination] = useState<Paginationparams<T>>({
    filter: filter,
    page: 0,
    size: 10,
    sort: []
  });
  const [pagination, setPagination] = useState<any>(null);

  return [filterPagination, setFilterPagination, pagination, setPagination];
}

export default usePagination;
