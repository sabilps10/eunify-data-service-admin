import { useState, useEffect } from "react";

const useDialog = (defaultValue = false) => {
  const [isOpen, setOpen] = useState(defaultValue);
  const [data, setData] = useState<any>(null);

  const onOpen = (data: any) => {
    setData(data);
    setOpen(true);
  };
  const onClose = () => {
    setData(null);
    setOpen(false);
  };

  useEffect(() => {
    setOpen(defaultValue);
  }, [defaultValue]);

  const dialogData = data;

  return {
    isOpen,
    onOpen,
    onClose,
    dialogData
  };
};

export default useDialog;
