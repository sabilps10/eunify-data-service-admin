import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import shallow from "zustand/shallow";
import get from "lodash/get";

import defaultLang from "@locales/en-US/translation.json";
import createTranslateStore, { TranslateStore } from "@store/translateStore";

const useTranslate = (namespace = "translation") => {
  const { t, ready, i18n } = useTranslation(namespace);
  const translateStore = createTranslateStore((state) => state, shallow) as TranslateStore;

  const translate = (key: string) => t(key, { defaultValue: get(defaultLang, key) });

  useEffect(() => {
    if (ready) {
      i18n.changeLanguage(translateStore.language);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ready, translateStore.language]);

  return {
    translate,
    ...translateStore
  };
};

export default useTranslate;
