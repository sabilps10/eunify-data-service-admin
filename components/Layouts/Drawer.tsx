import { FC, useState, useEffect, useCallback } from "react";
import { styled } from "@mui/material/styles";

import MuiDrawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import Toolbar from "@mui/material/Toolbar";
import { DRAWER_WIDTH, LOCALSTORAGE_KEY } from "@/config/constants";
import {
  ListItemButton,
  Typography,
  ListItemText,
  Stack,
  Skeleton,
} from "@mui/material";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import useDrawerStore from "@/store/drawerStore";
import { useRouter } from "next/router";
import useMounted from "@/hooks/useMounted";
import useLocalStorage from "@/hooks/useLocalStorage";
import shallow from "zustand/shallow";

const DrawerComponent = () => {
  const isDrawerOpen = useDrawerStore((state) => state.isDrawerOpen);
  const { isMounted } = useMounted();

  const [_, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);

  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);

  const { push, query } = useRouter();

  const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== "open" })(({ theme }) => {
    return {
      "& .MuiDrawer-paper": {
        position: "relative",
        whiteSpace: "nowrap",
        width: DRAWER_WIDTH,
        maxHeight: "100vh",
        overflow: "scroll",
        boxSizing: "border-box",
        [theme.breakpoints.down("md")]: {
          width: theme.spacing(9)
        }
      }
    };
  });

  const RenderListMenu = () => {
    if (!isMounted) return <></>;

    return (
      <List component="nav">
        <Stack>
          <ListItemButton
            // onClick={() => push("/report-management/management")}
            sx={{ paddingLeft: 3, whiteSpace: "normal" }}
          >
            <ListItemText primary={<Typography variant="h4">Bank Table</Typography>} />
          </ListItemButton>
          <ListItemButton
            // onClick={() => push("/report-management/management")}
            sx={{ paddingLeft: 3, whiteSpace: "normal" }}
          >
            <ListItemText primary={<Typography variant="h4">Occupation Table</Typography>} />
          </ListItemButton>
          <ListItemButton
            // onClick={() => push("/report-management/management")}
            sx={{ paddingLeft: 3, whiteSpace: "normal" }}
          >
            <ListItemText primary={<Typography variant="h4">Address Table</Typography>} />
          </ListItemButton>
        </Stack>
      </List>
    );
  };

  return (
    <Drawer variant="permanent" open={isDrawerOpen}>
      <Toolbar
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start"
        }}
      >
        <Stack direction={"row"} gap={2} alignItems="center">
          <AccountCircleIcon />
          <Typography variant="h4">{account}</Typography>
        </Stack>
      </Toolbar>
      <Divider />
      <RenderListMenu />
      <Stack sx={{ marginTop: 5, marginLeft: 3, marginBottom: 5 }}>
        {/* <Typography sx={{ color: "#adadad" }} variant={"body"}>
          {process.env.NEXT_PUBLIC_ENVIRONMENT}: v{process.env.NEXT_PUBLIC_VERSION}
        </Typography> */}
      </Stack>
    </Drawer>
  );
};

const RenderSkeletonLoading = () => {
  return (
    <Stack gap={2} sx={{ mt: 2 }}>
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={180} height={20} sx={{ borderRadius: 1, ml: 10 }} />
      <Skeleton variant="rectangular" width={180} height={20} sx={{ borderRadius: 1, ml: 10 }} />
      <Skeleton variant="rectangular" width={180} height={20} sx={{ borderRadius: 1, ml: 10 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={180} height={20} sx={{ borderRadius: 1, ml: 10 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
      <Skeleton variant="rectangular" width={220} height={20} sx={{ borderRadius: 1, ml: 3 }} />
    </Stack>
  );
};

export default DrawerComponent;
