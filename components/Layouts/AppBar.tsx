import { FC, useState, useEffect } from "react";
import { styled } from "@mui/material/styles";

import { DRAWER_WIDTH } from "@/config/constants";
import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import { Button, Typography } from "@mui/material";
import useDrawerStore from "@/store/drawerStore";
import { FiLogOut as LogoutIcon } from "react-icons/fi";
import useAmazonCognito from "@/hooks/useAmazonCognito";
import { useRouter } from "next/router";

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBarComponent: FC<{ isLoginPageOr404: boolean }> = ({ isLoginPageOr404 }) => {
  const router = useRouter();
  const isDrawerOpen = useDrawerStore((state) => state.isDrawerOpen);
  const setDrawer = useDrawerStore((state) => state.setDrawer);
  const { logout } = useAmazonCognito();

  const toggleDrawer = () => {
    setDrawer(!isDrawerOpen);
  };

  const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== "open"
  })<AppBarProps>(({ theme, open }) => ({
    zIndex: theme.zIndex.drawer + 1,
    [theme.breakpoints.up("md")]: {
      marginLeft: isLoginPageOr404 == false ? DRAWER_WIDTH : 0,
      width: `calc(100% - ${isLoginPageOr404 == false ? DRAWER_WIDTH : 0}px)`
    }
  }));
  const doLogout = () => {
    //@ts-ignore
    logout();
    router.push("/login");
  };

  return (
    <AppBar
      position="absolute"
      open={isDrawerOpen}
      sx={() => ({
        boxShadow: 0
      })}
    >
      <Toolbar
        sx={(theme) => ({
          background: theme.palette.brandYellow[500],
          pr: "24px"
        })}
      >
        <Typography variant="h3" color="dark" noWrap sx={{ flexGrow: 1 }}>
          Data Service Admin Dashboard
        </Typography>
        {router.pathname !== "/login" && (
          <Button onClick={doLogout}>
            <LogoutIcon size={20} />
            <Typography variant="body" sx={{ fontSize: 18, ml: 1, fontWeight: "bold" }}>
              Logout
            </Typography>
          </Button>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default AppBarComponent;
