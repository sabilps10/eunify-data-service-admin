import { FC, useState, PropsWithChildren } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { Stack, Typography } from "@mui/material";

type ModalDialogProps = PropsWithChildren<{
  title?: string;
  isOpen: boolean;
  onClose?: () => void;
  onCallback?: () => void;
  buttons: { continue: string; cancel: string };
  data?: any;
  sx?: any;
  fullwidth?: boolean;
  disabledContinue?: boolean;
}>;

const ModalDialog: FC<ModalDialogProps> = ({
  isOpen = false,
  title = "",
  onClose = () => null,
  onCallback = () => null,
  data = null,
  buttons = { continue: "Confirm", cancel: "Cancel" },
  children,
  sx,
  fullwidth,
  disabledContinue
}) => {
  return (
    <Dialog open={isOpen} onClose={onClose} maxWidth={"md"}>
      <DialogContent>
        <Typography variant="h4" fontWeight={700}>
          {title}
        </Typography>
        <Stack sx={{ mt: 3 }}>{children}</Stack>
      </DialogContent>
      <DialogActions sx={{ mr: 2, ml: 2, mb: 2 }}>
        <Button variant={"outlined"} onClick={onClose}>
          <Typography variant={"body"} fontWeight="bold">
            {buttons.cancel}
          </Typography>
        </Button>
        <Button onClick={onCallback} disabled={disabledContinue}>
          <Typography variant={"body"} fontWeight="bold">
            {buttons.continue}
          </Typography>
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ModalDialog;
