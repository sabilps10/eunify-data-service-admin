import { FC } from "react";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { DesktopDatePicker, LocalizationProvider, TimePicker } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { Typography, Stack, TextField, FormControl, FormLabel, TextFieldProps } from "@mui/material";

import { Controller, useFormContext } from "react-hook-form";

import useTranslate from "@/hooks/useTranslate";

export type InputTimeProps = {
  defaultValue: any;
  name: string;
  label: string;
  required?: boolean;
  get?: any;
  set?: any;
  watch?: any;
} & TextFieldProps;

const InputTime: FC<InputTimeProps> = ({
  name,
  label,
  defaultValue,
  required = false,
  get,
  set,
  watch,
  ...props
}) => {
  const {
    control,
    formState: { errors }
  } = useFormContext();
  const { translate } = useTranslate();


  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Stack direction="column" gap={1}>
        <Stack direction="row" alignItems="center" gap={2}>
          <Controller
            name={name}
            control={control}
            defaultValue={defaultValue}
            render={({ field: { onChange, value }, fieldState: { error } }) => {
              return (
                <FormControl sx={{ gap: 1, width: "100%" }}>
                  {label && <FormLabel color="secondary">{label}</FormLabel>}
                  <TimePicker
                    inputFormat="HH:mm:ss"
                    value={watch(name)}
                    onChange={(e) => {
                      set(name, e);
                    }}
                    renderInput={(params) => (
                      <TextField
                        size="small"
                        {...params}
                        {...props}
                        required
                        sx={{ height: 56, justifyContent: "center" }}
                      />
                    )}
                  />
                  <Typography variant="body" color="brandRed.500">{`${
                    error ? error.message : ""
                  }`}</Typography>
                </FormControl>
              );
            }}
          />
        </Stack>
      </Stack>
    </LocalizationProvider>
  );
};

export default InputTime;
