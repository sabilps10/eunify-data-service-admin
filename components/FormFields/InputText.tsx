import { FC, useState, PropsWithChildren, useEffect, useMemo, useCallback, forwardRef } from "react";
import { useRouter } from "next/router";
import { debounce } from "lodash";

import {
  TextField,
  FormLabel,
  TextFieldProps,
  Typography,
  FormControl,
  Stack,
  MenuItem,
  Button,
  Snackbar,
  Alert,
  AlertTitle
} from "@mui/material";
import { Controller, useFormContext } from "react-hook-form";
import useTranslate from "@/hooks/useTranslate";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

import NumberFormat from "react-number-format";

interface CustomNumberFormatProps {
  onChange: (event: { target: { name: string; value: any } }) => void;
  name: string;
  allowNegative: boolean;
  isNumberCurrency: boolean;
  decimalScale: number;
}

const CustomNumberFormat = forwardRef<NumberFormat<any>, CustomNumberFormatProps>(function NumberFormatCustom(
  props,
  ref
) {
  const { onChange, ...other } = props;
  return (
    <NumberFormat
      {...other}
      allowNegative={props.allowNegative}
      decimalScale={props.decimalScale || 2}
      getInputRef={ref}
      isAllowed={(values) => {
        if (props.isNumberCurrency) {
          const { value, floatValue } = values;
          if (typeof floatValue === "undefined" || typeof value === "undefined") {
            return true;
          }
          if (value.charAt(0) === "0") {
            if (value.charAt(1) && value.charAt(1) != ".") {
              return false;
            }
          }
          return true;
        } else {
          return true;
        }
      }}
      onValueChange={(evt) => {
        onChange({
          target: {
            name: props.name,
            value: evt.floatValue ? evt.floatValue.toString() : evt.value
          }
        });
      }}
      thousandSeparator
    />
  );
});

export type InputTextProps = {
  label?: string;
  fullWidth?: boolean;
  required?: boolean;
  name: string;
  sx?: any;
  type?: string;
  size?: string;
  allowNegative?: boolean;
  decimalScale?: number;
  defaultValue?: any;
  validateInput?: boolean;
  additionalValue?: any;
  additionalOptions?: any[];
  additionalRegex?: any;
  addQuery?: boolean;
  set?: any;
  get?: any;
  formData?: any;
  setValue?: () => void;
  parentCallback?: () => void;
} & TextFieldProps;

const InputText: FC<InputTextProps> = ({
  label,
  fullWidth,
  required,
  name,
  sx,
  type,
  size,
  allowNegative = false,
  decimalScale,
  defaultValue,
  additionalValue,
  validateInput,
  additionalOptions,
  additionalRegex,
  addQuery = false,
  set = () => null,
  get = () => null,
  formData,
  parentCallback = () => null,
  ...props
}) => {
  const {
    control,
    formState: { errors },
    getValues
  } = useFormContext();

  const [_, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);

  const { query, push } = useRouter();
  const [paymentUnitOptions, setPaymentUnitOptions] = useState<any[]>([]);

  const error = errors[name] ? errors[name]?.message : "";
  const { translate } = useTranslate();
  const isNumberInput = useMemo(() => {
    if (!type) return false;
    return type.includes("number");
  }, [type]);

  const handleChange = (val: any) => {
    let value = val;
    if(additionalRegex != false){
      value = val.replaceAll(additionalRegex ? additionalRegex : /[`#$%^&*"_+=\[\]{};':"\\|<>\/?~]/g, "");
    }
    if (type == "number-currency") {
      value = val == "" ? "0" : val;
    }
    set(name, value, { shouldValidate: validateInput });
    if (addQuery) {
      push({ pathname: `/${query.feature}/${query.page}`, query: { ...query, [name]: val } });
    }
    return;
  };

  const onChangeChargeUnit = (event: any) => {
    if (name == "service_fee_charge") {
      set("service_fee_unit", event.target.value, { shouldValidate: validateInput });
    }
    if (name == "gateway_charge") {
      set("gateway_charge_unit", event.target.value, { shouldValidate: validateInput });
    }
  };

  const onChangeCurrency = (event: any) => {
    set("currency", event.target.value), { shouldValidate: validateInput };
    setTimeout(() => {
      if (parentCallback) {
        parentCallback();
      }
    }, 200);
  };

  const RenderNumberCurrency = () => {
    if (additionalValue === null) {
      return (
        <TextField
          {...props}
          select
          onChange={onChangeCurrency}
          sx={{ width: 210 }}
          value={get("currency") || ""}
        >
          {additionalOptions &&
            additionalOptions?.map((option: any, index: number) => {
              return (
                <MenuItem value={option.value} key={index}>
                  <Typography fontWeight={700}>{option.label}</Typography>
                </MenuItem>
              );
            })}
        </TextField>
      );
    }

    if (Array.isArray(additionalValue)) additionalValue = additionalValue[0];
    return <TextField disabled value={additionalValue ? additionalValue : ""} sx={{ width: 210 }} />;
  };
  
  return (
    <>
      <Controller
        control={control}
        name={name}
        defaultValue={defaultValue}
        render={({ field }) => {
          return (
            <>
              <FormControl sx={{ gap: 1 }} fullWidth error={!!errors[name]}>
                {label && <FormLabel color="secondary">{label}</FormLabel>}
                <Stack direction={"row"} gap={1} alignItems="center">
                  {type == "number-currency" && <RenderNumberCurrency />}
                  <TextField
                    {...props}
                    {...field}
                    value={field.value}
                    sx={{
                      border: "solid",
                      borderWidth: !!errors[name] ? 1 : 0,
                      borderColor: "red",
                      ...sx
                    }}
                    fullWidth={fullWidth}
                    required={required}
                    size={size}
                    multiline={type == "textarea" ? true : false}
                    rows={type == "textarea" ? 4 : 0}
                    type={name === "password" ? "password" : undefined}
                    onChange={(e) => handleChange(e.target.value)}
                  />
               
                </Stack>
                <Typography variant="body" color="brandRed.500">{`${error}`}</Typography>
              </FormControl>
            </>
          );
        }}
      ></Controller>
    </>
  );
};

export default InputText;
