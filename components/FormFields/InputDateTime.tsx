import { FC } from "react";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { DatePicker, DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { Typography, Stack, TextField, FormControl, FormLabel, TextFieldProps } from "@mui/material";

import { Controller, useFormContext } from "react-hook-form";

import useTranslate from "@/hooks/useTranslate";

export type InputDateProps = {
  defaultValue: any;
  name: string;
  label: string;
  required?: boolean;
  set?: any;
  watch?: any;
  disabled: boolean;
  inputFormat?: any;
  viewFormat?: any;
} & TextFieldProps;

const InputDateTime: FC<InputDateProps> = ({
  name,
  label,
  defaultValue,
  disabled,
  set,
  watch,
  required = false,
  inputFormat,
  viewFormat,
  ...props
}) => {
  const {
    control,
    formState: { errors }
  } = useFormContext();
  const { translate } = useTranslate();

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Stack direction="column" gap={1}>
        <Stack direction="row" alignItems="center" gap={2}>
          <Controller
            name={name}
            control={control}
            defaultValue={defaultValue}
            render={({ field: { onChange, value }, fieldState: { error } }) => {
              return (
                <FormControl sx={{ gap: 1, width: "100%" }}>
                  {label && <FormLabel color="secondary">{label}</FormLabel>}
                  <DatePicker
                    inputFormat={inputFormat ? inputFormat : "yyyy-MM-dd"}
                    views={viewFormat ? viewFormat : ["year", "day"]}
                    value={value}
                    onAccept={(value) => {
                      set(name, value);
                    }}
                    disabled={disabled}
                    onChange={() => null}
                    minDate={new Date(defaultValue)}
                    renderInput={(params) => (
                      <TextField
                        size="small"
                        {...params}
                        inputProps={{ ...params.inputProps, placeholder: "Select Date Time" }}
                      />
                    )}
                  />
                  <Typography variant="body" color="brandRed.500">{`${
                    error ? error.message : ""
                  }`}</Typography>
                </FormControl>
              );
            }}
          />
        </Stack>
      </Stack>
    </LocalizationProvider>
  );
};

export default InputDateTime;
