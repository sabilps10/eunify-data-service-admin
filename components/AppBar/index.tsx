import type { FC, PropsWithChildren } from "react";
import Box from "@mui/material/Box";
import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";

export type AppBarProps = PropsWithChildren<MuiAppBarProps>;

const AppBar: FC<AppBarProps> = ({ children, ...props }) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <MuiAppBar position="static" {...props}>
        <Toolbar>{children}</Toolbar>
      </MuiAppBar>
    </Box>
  );
};

export default AppBar;
