const convertNameToUrl = (value: string) => {
  value = value.split("/").join("-");
  return value.toLocaleLowerCase().split(" ").join("-");
};

export default convertNameToUrl;
