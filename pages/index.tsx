import type { NextPage } from "next";
import { getUnixTime } from "date-fns";
import { LOCALSTORAGE_KEY } from "@/config/constants";
import useLocalStorage from "@/hooks/useLocalStorage";
import useAmazonCognito from "@/hooks/useAmazonCognito";
import { useEffect } from "react";

const IndexPage: NextPage = () => {
  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const expired = getItem(LOCALSTORAGE_KEY.EXPIRED_TIME);
  const { logout } = useAmazonCognito(); // eslint-disable-line react-hooks/rules-of-hooks

  useEffect(() => {
    const currTime = getUnixTime(new Date());
    if (Number(currTime) >= Number(expired)) {
      logout();
    }
  }, []);

  return null
  // <strong>Welcome Back {account}!</strong>;
};

export default IndexPage;
