import type { NextPage } from "next";
import type { AppProps } from "next/app";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import useAmazonCognito from "@/hooks/useAmazonCognito";
import TranslateProvider from "@providers/TranslateProvider";
import MuiThemeProvider from "@/providers/MuiThemeProvider";
import ReactQueryProvider from "@providers/ReactQueryProvider";
import LayoutProvider, { LayoutProviderProps } from "@providers/LayoutProvider";
import BaseLayout from "@/components/Layouts/BaseLayout";
import { LOCALSTORAGE_KEY } from "@/config/constants";

export type NextPageWithProvider<Props = {}> = NextPage<Props> & LayoutProviderProps;

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithProvider;
};

function MIOPortalApp({ Component, pageProps }: AppPropsWithLayout) {
  const [status] = useState<boolean>(false);

  return (
    <TranslateProvider>
      <MuiThemeProvider>
        <LayoutProvider isAuthPage={status}>
          <ReactQueryProvider>
            {typeof window !== "undefined" && (
              <BaseLayout>
                <Component {...pageProps} />
              </BaseLayout>
            )}
          </ReactQueryProvider>
        </LayoutProvider>
      </MuiThemeProvider>
    </TranslateProvider>
  );
}

export default MIOPortalApp;
