import Head from "next/head";

import { NextPageWithProvider } from "@pages/_app";
import useTranslate from "@/hooks/useTranslate";
import { useRouter } from "next/router";
import DashboardList from "@/features/dashboard/List";

const List: NextPageWithProvider = () => {
  const { translate } = useTranslate();
  const { query } = useRouter();

  return (
    <>
      <Head>
        <title>{translate("dashboard.title")}</title>
      </Head>
      <DashboardList />
    </>
  );
};

export default List;
